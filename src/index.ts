import bodyParser from 'body-parser';
import express from 'express';
import { createPost, deletePost, getAllPosts, getOnePost, updatePost } from './persistence';

const PORT = 3000;

/**
 * Создаем Express-сервер
 */
export const app = express();

/**
 * Это мидлвара, которая позволяет читать BODY, который передали как JSON-объект
 */
app.use(bodyParser.json());

/**
 * Пример одного эдпоинта, используйте его как образец
 */
 app.post('/posts', async (req, res) => {
  const post = await createPost(req.body);

  res.json(post);
});

app.get('/posts', async (_, res) => {
  const posts = await getAllPosts();

  res.json(posts);
});

app.get('/posts/:id', async (req, res) => {
  const post = await getOnePost(Number(req.params.id));

  if (!post) {
    res.status(404).send('Post not found.');
  }

  res.json(post);
});

app.patch('/posts/:id', async (req, res) => {
  const updatedPost = await updatePost(Number(req.params.id), req.body);

  res.json(updatedPost);
})

app.delete('/posts/:id', async (req, res) => {
  const deletedPost = await deletePost(Number(req.params.id));

  res.json(deletedPost);
})


/**
 * Чтобы во время тестов ваше запущенное приложение не конфликтовало
 *  с тем, которое поднимается для тестов
 */
if (process.env.NODE_ENV !== 'test') {
  /**
   * Обратите внимание что делает этот метод -- мы подписываемся на весь трафик
   *  с порта 3000, "слушаем его"
   */
  app.listen(PORT, () => {
    console.log(`Express application started on http://localhost:${PORT}`);
  });
}
