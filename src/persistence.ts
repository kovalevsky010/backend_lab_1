import { readFile, writeFile } from 'fs/promises';
import { DB_PATH } from './constants';
import { CreatePostDto, Post, UpdatePostDto } from './types';


async function getUniquePostId() {
  const posts = await getAllPosts();

  const maxId = Math.max(...posts.map((post) => post.id));

  return maxId + 1;
}

export async function getAllPosts(): Promise<Post[]> {
  
  try {
    const file = await readFile(DB_PATH);
    const fileContent = file.toString();

    if (!fileContent) {
      return [];
    }

    const posts: Post[] = JSON.parse(fileContent);

    return posts;
  } catch (e) {
    throw e;
  }
}

export async function getOnePost(id: number): Promise<Post | undefined > {
  try {
    const posts: Post[] = await getAllPosts();
    const post = posts.find((p) => p.id === id);
    
    return post;
  } catch (e) {
    throw e;
  }
}

export async function createPost(postDto: CreatePostDto): Promise<Post> {
  try {

    const posts = await getAllPosts();
    
    const newPost = {
      ...postDto,
      id: await getUniquePostId(),
      createdAt: new Date().toISOString(),
      updatedAt: new Date().toISOString()
    } as Post;

    posts.push(newPost);
    
    const postsJson = JSON.stringify(posts);
    writeFile(DB_PATH, postsJson);

    return newPost;
  } catch (e) {
    throw e;
  }
}

export async function updatePost(id: number, postDto: UpdatePostDto): Promise<Post> {
  try {
    const posts = await getAllPosts();
    const updatedPost = posts.find((post) => post.id === id);
    
    const afterUpdate = {
      ...updatedPost,
      ...postDto,
      updatedAt: new Date().toISOString()
    } as Post;
  
    const updatedPosts = posts.map((post) =>
      post.id === id ? afterUpdate : post
    ) 

    writeFile(DB_PATH, JSON.stringify(updatedPosts));
  
    return afterUpdate;
  } catch (e) {
    throw e;
  }
}

export async function deletePost(id: number): Promise<boolean> {
  try {
    const posts = await getAllPosts();
    
    const filteredPosts = posts.filter((post) => id !== post.id);

    writeFile(DB_PATH, JSON.stringify(filteredPosts));

    return true;
  } catch (e) {
    return false;
  }
}